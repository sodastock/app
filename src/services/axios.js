import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "http://localhost/api",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
});

axiosInstance.interceptors.request.use(
  function(config) {
    const token = window.localStorage.token;
    if (token) config.headers.Authorization = `Bearer ${token}`;

    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);

export const api = {
  get(endpoint) {
    return axiosInstance.get(endpoint);
  },
  post(endpoint, payload) {
    return axiosInstance.post(endpoint, payload);
  },
  patch(endpoint, payload) {
    return axiosInstance.patch(endpoint, payload);
  },
  delete(endpoint) {
    return axiosInstance.delete(endpoint);
  },
  login(payload) {
    return axiosInstance.post("/login", payload);
  },
  register(payload) {
    return axiosInstance.post("/register", payload);
  }
};
