import Vue from "vue";
import SuiVue from "semantic-ui-vue";
import VueSweetalert2 from "vue-sweetalert2";
import VueToastr from "vue-toastr";
import router from "./routes";
import store from "./store";
import App from "./App.vue";

import "semantic-ui-css/semantic.min.css";
import "sweetalert2/dist/sweetalert2.min.css";

import AppHeader from "@/components/UI/AppHeader";
import AppFooter from "@/components/UI/AppFooter";
import Modal from "@/components/Util/Modal";
import ErrorBox from "@/components/Util/ErrorBox";
import Search from "@/components/Util/Search";

Vue.use(SuiVue);
Vue.use(VueSweetalert2);
Vue.use(VueToastr);

// Global components
Vue.component("AppHeader", AppHeader);
Vue.component("AppFooter", AppFooter);
Vue.component("Modal", Modal);
Vue.component("ErrorBox", ErrorBox);
Vue.component("Search", Search);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
