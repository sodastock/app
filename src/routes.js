import Vue from "vue";
import Router from "vue-router";

import Dashboard from "@/views/Dashboard";
import Register from "@/views/Register";
import Login from "@/views/Login";
import Modal from "@/components/Util/Modal";

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    {
      name: "dashboard",
      path: "/",
      component: Dashboard,
      meta: {
        auth: true
      }
    },
    {
      name: "create",
      path: "/soda/create",
      component: Modal,
      meta: {
        auth: true
      }
    },
    {
      name: "edit",
      path: "/soda/:id",
      component: Modal,
      meta: {
        auth: true
      }
    },
    {
      name: "login",
      path: "/login",
      component: Login
    },
    {
      name: "register",
      path: "/register",
      component: Register
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.auth)) {
    if (!window.localStorage.token) {
      next("/login");
    }
    next();
  }

  next();
});

export default router;
