import * as types from "./mutations-types";

export default {
  [types.GET_SODA](state, soda) {
    state.soda = soda;
  },
  [types.GET_SODAS](state, sodas) {
    state.sodas = sodas;
  },
  [types.STORE_SODA](state, soda) {
    state.sodas.push(soda);
  },
  [types.UPDATE_SODA](state, payload) {
    const index = state.sodas.findIndex(soda => soda.id == payload.id);
    state.sodas[index] = payload;
  },
  [types.DELETE_SODA](state) {
    state.trash.forEach(sodaId => {
      const index = state.sodas.findIndex(soda => soda.id == sodaId);
      state.sodas.splice(index, 1);
    });
    state.trash = [];
  },
  [types.CLEAR_SODA](state) {
    state.soda = {};
  },
  // Others
  [types.SET_ERRORS](state, errors) {
    Object.keys(errors).forEach(key => state.errors.push(errors[key][0]));
  },
  [types.CLEAR_ERRORS](state) {
    state.errors = [];
  },
  [types.LOADING_SODAS](state, loading) {
    state.loading = loading;
  },
  [types.SELECTED_SODAS](state, selected) {
    state.trash = selected;
  },
  [types.SELECT_ALL_SODAS](state, select) {
    state.trash = [];
    if (select) state.sodas.forEach(soda => state.trash.push(soda.id));
  },
  [types.NOTIFICATION](state, alert) {
    state.alert = {
      message: alert.message,
      type: alert.type
    };
  },
  [types.NOTIFICATION_CLEAR](state) {
    state.alert = {
      message: "",
      type: ""
    };
  },
  [types.SET_PAGINATION](state, pagination) {
    const { current_page, total_page, total_items } = pagination;

    state.pagination = {
      current_page,
      total_page,
      total_items
    };
  },
  [types.SET_USER](state, email) {
    state.user = { email };
  },
  [types.CLEAR_USER](state) {
    state.user = { email: "" };
  }
};
