export default {
  loading: false,
  sodas: [],
  soda: {
    brand: "",
    volume: "",
    type: "",
    cost: "",
    quantity: ""
  },
  trash: [], //sodasId
  alert: {
    message: "",
    type: ""
  },
  errors: [],
  pagination: {
    current_page: 1,
    total_page: 1,
    total_items: 1
  },
  user: {
    email: ""
  }
};
