import * as types from "./mutations-types";
import { api } from "@/services/axios";

export const getSoda = ({ commit }, payload) => {
  commit(types.LOADING_SODAS, true);

  api.get(`/soda/${payload}`).then(response => {
    commit(types.GET_SODA, response.data.data);
    commit(types.LOADING_SODAS, false);
  });
};

export const getSodas = ({ commit }, payload) => {
  commit(types.LOADING_SODAS, true);

  const { page, search } = payload;

  let searchQuery = "";
  if (search)
    searchQuery = `search=${search.query}&searchFields=${search.field}&`;

  api.get(`/soda?${searchQuery}page=${page}`).then(response => {
    const pagination = {
      current_page: response.data.current_page,
      total_page: response.data.last_page,
      total_items: response.data.total
    };

    commit(types.SET_PAGINATION, pagination);
    commit(types.GET_SODAS, response.data.data);
    commit(types.LOADING_SODAS, false);
  });
};

export const storeSoda = ({ commit }, payload) => {
  commit(types.LOADING_SODAS, true);
  commit(types.CLEAR_ERRORS);

  api
    .post("/soda", payload)
    .then(response => {
      commit(types.STORE_SODA, response.data.data);
      commit(types.LOADING_SODAS, false);
      commit(types.NOTIFICATION, {
        message: "Adicionado com sucesso!",
        type: "success"
      });
    })
    .catch(error => {
      const errors = error.response.data.errors;

      commit(types.LOADING_SODAS, false);
      commit(types.SET_ERRORS, errors);
    });
};

export const updateSoda = ({ commit, state }, payload) => {
  commit(types.LOADING_SODAS, true);
  commit(types.CLEAR_ERRORS);

  const id = state.soda.id;

  api
    .patch(`/soda/${id}`, payload)
    .then(() => {
      commit(types.UPDATE_SODA, payload);
      commit(types.LOADING_SODAS, false);
      commit(types.CLEAR_SODA);
      commit(types.NOTIFICATION, {
        message: "Atualizado com sucesso!",
        type: "success"
      });
    })
    .catch(error => {
      const errors = error.response.data.errors;

      commit(types.LOADING_SODAS, false);
      commit(types.SET_ERRORS, errors);
    });
};

export const deleteSoda = ({ commit, state, dispatch }) => {
  const ids = state.trash.join(",");

  api.delete(`/soda/${ids}`).then(() => {
    commit(types.DELETE_SODA);
    commit(types.NOTIFICATION, {
      message: "Excluído com sucesso!",
      type: "success"
    });
    dispatch("getSodas", { page: 1 });
  });
};

export const clearSoda = ({ commit }) => {
  commit(types.CLEAR_SODA);
};

export const setErrors = ({ commit }, payload) => {
  commit(types.SET_ERRORS, payload);
};

export const loadingSodas = ({ commit }, payload) => {
  commit(types.LOADING_SODAS, payload);
};

export const selectedSodas = ({ commit }, payload) => {
  commit(types.SELECTED_SODAS, payload);
};

export const selectAllSodas = ({ commit }, payload) => {
  commit(types.SELECT_ALL_SODAS, payload);
};

export const clearNotification = ({ commit }) => {
  commit(types.NOTIFICATION_CLEAR);
};

/**
 * Auth
 */
export const login = ({ commit }, payload) => {
  commit(types.LOADING_SODAS, true);

  api
    .login(payload)
    .then(response => {
      window.localStorage.token = response.data.access_token;
      commit(types.SET_USER, payload.email);
      commit(types.LOADING_SODAS, false);
      commit(types.NOTIFICATION, {
        message: "Login efetuado com sucesso!",
        type: "success"
      });
    })
    .catch(() => {
      commit(types.LOADING_SODAS, false);
      commit(types.NOTIFICATION, {
        message: "Erro ao fazer o login!",
        type: "error"
      });
    });
};

export const register = ({ commit }, payload) => {
  commit(types.LOADING_SODAS, true);

  api
    .register(payload)
    .then(response => {
      window.localStorage.token = response.data.access_token;
      commit(types.SET_USER, payload.email);
      commit(types.LOADING_SODAS, false);
      commit(types.NOTIFICATION, {
        message: "Cadastrado com sucesso!",
        type: "success"
      });
    })
    .catch(() => {
      commit(types.LOADING_SODAS, false);
      commit(types.NOTIFICATION, {
        message: "Usuário já existe!",
        type: "error"
      });
    });
};

export const logout = ({ commit }) => {
  commit(types.SET_USER, "");
  commit(types.NOTIFICATION, {
    message: "Deslogado com sucesso!",
    type: "success"
  });
  window.localStorage.removeItem("token");
};
