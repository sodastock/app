<table align="center"><tr><td align="center" width="9999">
<img src="documentation/soda.jpg" align="center" width="150" alt="SodaStock">

# SodaStock

SodaStock is a stock control system for the management of refrigerants. Developed for Fortics FullStack Developer position.

</td></tr></table>

## Table of Contents

- [Tools](#tools)

- [Database](#database)

- [Features](#features)

### Run

`docker run -it --rm -v "$PWD":/usr/src/app -w /usr/src/app node:12 npm install`

`docker-compose up -d`

### Tools

- Vue-cli 3
- Semantic-vue
- Sweetalert2
- Toastr
- Axios
- Vuex
- Vue-router
- Sass

### Database

![Sodas](documentation/soda_db.jpg "Sodas")
![Users](documentation/user_db.jpg "Users")

### Features

- Create soda

![Create](documentation/create.gif "Create")

![Create](documentation/create_error.gif "Create")

- Update soda

![Update](documentation/update.gif "Update")

- Delete soda

![Delete](documentation/delete.gif "Delete")

![Delete](documentation/deleteM.gif "Delete")

- Pagination

![Pagination](documentation/pagination.gif "Pagination")

- Search

![Search](documentation/search.gif "Search")

![Search](documentation/search_other.gif "Search")

- Register

![Register](documentation/register.gif "Register")

- Login

![Login](documentation/login_success.gif "Login")

![Login](documentation/login_error.gif "Login")

- Logout

![Logout](documentation/logout.gif "Logout")
